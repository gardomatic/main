#include "LowPower.h"

void setup()
{
  pinMode(7,OUTPUT);
}

void loop() 
{
  digitalWrite(7,HIGH);
  delay(2000);
  digitalWrite(7,LOW);
  LowPower.powerDown(SLEEP_8S, ADC_OFF, BOD_OFF);
}
